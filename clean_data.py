
# Use for cleaning up some of the data converting csv with index by id and index by time
import pandas as pd
import os

data_dir = os.path.join(os.getcwd(), "data")
files = os.listdir(data_dir)

for file_name in files:
    print(file_name)
    file_path = os.path.join(data_dir, file_name)
    df = pd.read_csv(file_path, index_col=[0], parse_dates=True, infer_datetime_format=True)
    print(df.head()) 
    if any(df.columns.str.match('time')):
        df.to_csv(file_path, index=False)
